package сlinicbot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import сlinicbot.entity.client.Client;
import сlinicbot.db.DataBase;

import java.util.Arrays;

public class ClinicBot extends TelegramLongPollingBot {
    ClientRepository clientRepository = new ClientRepository();
    Client currentClient;

    // TODO: 10/10/20 убрать инициализацию currentUser, добавить каждый раз поиск по id
    public void onUpdateReceived(Update update) {
        String text = update.getMessage().getText();
        User user = update.getMessage().getFrom();
        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            Client client = clientRepository.findClientById(user.getId());
            SendMessage message = new SendMessage().setChatId(update.getMessage().getChatId());
            if (client == null) {
                clientRepository.addClient(Client.ClientBuilder.build(user));
                message.setText(sayHello(user.getFirstName()));
                client = clientRepository.findClientById(user.getId());
                DataBase.addNewClient(user);
                currentClient = client;
            }
            // проверяем, пишет ли юзер про город
            else if (Arrays.stream(Cities.values()).anyMatch(city -> text.contains(city.getCity()))) {
                client.setCity(text);
                message.setText("Хорошо, я записал, спасибо! По какому адресу у тебя клиника?");
            }

            // проверяем, пишет ли юзер про адресс
            else if (Arrays.stream(ClinicsAddresses.values()).anyMatch(clinicsAddresses -> text.contains(clinicsAddresses.getAddress()))) {
                client.setAddress(text);
                // TODO: 10/10/20 добавить услуги для конкретного учреждения
                // TODO: 10/10/20 создать сущность учреждения с адресом и услугами
                message.setText("Спасибо! Какая услуга тебя интересует?");
            }
//            else if (Arrays.stream(Clinic.Procedure.values()).anyMatch(procedure -> text.contains(procedure.getProcedure()))) {
//                client.addRequestedProcedure(text);
//                message.setText("Хорошо. Тебя записать или вывести информацию о процедуре?");
//            }
            else {
                message.setText("Я тебя не понял, попробуй ответить по-другому");
            }

//            try {
//                DataBase.createUser(user.getId(), user.getFirstName(), user.getBot(), user.getLastName(), user.getUserName(), user.getLanguageCode());
//            } catch (SQLException throwables) {
//                throwables.printStackTrace();
//            }
            System.out.println(update.toString());
            clientRepository.addClient(client);
            try {
                execute(message); // Call method to send the message
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    public String getBotUsername() {
        return "reminder";
    }

    public String getBotToken() {
        return "1346520766:AAHyN9E86lnbx5_0w7OXVdZ8CCGjNN9GK6k";
    }

    public String sayHello(String name) {
        return "Привет, " + name + "! Ты из Москвы или СБП?";
    }

}
