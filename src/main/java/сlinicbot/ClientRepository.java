package сlinicbot;

import сlinicbot.entity.client.Client;

import java.util.HashMap;
import java.util.Map;

public class ClientRepository {
    private Map<Integer, Client> clients = new HashMap<>();

    public Map<Integer, Client> getClients() {
        return clients;
    }

    public Client findClientById(Integer id) {
        return clients.get(id);
    }

//    public Client getClientFromDB(User user) throws SQLException {
//        DataBase.createUser(user.getId(), user.getFirstName(), user.getBot(), user.getLastName(), user.getLastName(), user.getLanguageCode());
//    }

    public void addClient(Client client) {
        clients.put(client.getId(), client);
    }

    // TODO: 10/10/20 добавить выгрузку из БД
}
