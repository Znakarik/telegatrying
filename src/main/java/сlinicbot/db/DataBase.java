package сlinicbot.db;

import org.telegram.telegrambots.meta.api.objects.User;
import сlinicbot.ClinicLogger;
import сlinicbot.entity.Clinic;
import сlinicbot.entity.Procedure;
import сlinicbot.entity.client.Client;

import java.sql.Connection;
import java.sql.SQLException;

import java.sql.*;
import java.util.logging.Level;

public class DataBase {
    private static final String DB_URL = "jdbc:sqlite:/Users/znakarik/Library/DBeaverData/workspace6/.metadata/sample-database-sqlite-1/ClinicBot.db";

//    public static void main(String[] args) throws SQLException, ClassNotFoundException {
//
////
////        Connection connection = DriverManager.getConnection(DB_URL, "user", "");
////        String Sql = "INSERT INTO USERS (ID, FIRST_NAME) VALUES (1, 'Olya')";
////        Statement statement = connection.createStatement();
////        statement.execute(Sql);
////        connection.close();
////        DataBase.print();
//        DriverManager.registerDriver(new org.sqlite.JDBC());
//        Connection connection = DriverManager.getConnection(DB_URL, "user", "");
//
////        String SQL = DataBase.createUserReturnString(225878130,"Helga",Boolean.parseBoolean("0"),"Paulson","Znakarik","en");
//        String SQL = "SELECT * FROM USERS WHERE ID='225878130'";//,"Helga",Boolean.parseBoolean("0"),"Paulson","Znakarik","en");
//        Statement statement = connection.createStatement();
//        ResultSet resultSet = statement.executeQuery(SQL);
//        Client client = mapClientFromDBToObject(resultSet.getInt(1), resultSet.getString(2), resultSet.getBoolean(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6));
//
//        System.out.println(client);
//    }

    public static void main(String[] args) {
        Client client = Client.ClientBuilder.build(432674130, "Кольга", false, "Кремов", "Крин", "ru");
//        Clinic clinic = new Clinic("Барклая 8", "Пелагея");
        // TODO: 10/14/20 похоже надо в SQL добавлять кавычки для стрингов
        DataBase.addNewClient(client);
    }

    /**
     * Маппим строку USERS в Client
     *
     * @return new Client
     */
    // TODO: 10/11/20 использовать этот метод, если в Clients не находит по id
    public static Client mapClientFromDBToObject(Integer id, String firstName, Boolean isBot, String lastName, String userName, String languageCode) {
        User user = new User(id, firstName, isBot, lastName, userName, languageCode);
        return Client.ClientBuilder.build(user);
    }

    /**
     * Добавляем нового клиента
     */
    public static void addNewClient(User user) {
        String selectWhere = SQLStatements.simpleSelectWhere("USERS", "ID", user.getId());
        if (!isRowExistsBySelect(selectWhere)) {
            String SQL = SQLStatements.addNewClientSQL(user);
            executeSQL(SQL);
            ClinicLogger.LOGGER.log(Level.INFO, "В CLIENTS добавлен новый клиент");
            return;
        }
        ClinicLogger.LOGGER.log(Level.WARNING, "В CLIENTS найден клиент с ID " + user.getId());
    }

    /**
     * Добавляем новую клинику
     */
    public static void addNewClinic(Clinic clinic) {
        String selectWhere = SQLStatements.simpleSelectWhere("CLINICS", "NAME", clinic.getName());
        if (!isRowExistsBySelect(selectWhere)) {
            String SQL = SQLStatements.addNewClinicWithoutProceduresList(clinic);
            executeSQL(SQL);
            ClinicLogger.LOGGER.log(Level.INFO, "В CLINICS добавлена новая клиника");
            return;
        }
        ClinicLogger.LOGGER.log(Level.WARNING, "В CLINICS найдена клиника \"" + clinic.getName() + "\"");
    }

    /**
     * Добавляем новый тип процедуры
     */
    public static void addNewProcedureType(Procedure procedure) {
        String SQL = SQLStatements.addNewProcedureToProcedureTypes(procedure);
        executeSQL(SQL);
    }

    /**
     * Добавляем новый тип процедуры
     */
    public static void addClinicProcedure(Procedure procedure, Clinic clinic) {
        String SQL = SQLStatements.addClinicProcedure(procedure, clinic);
        executeSQL(SQL);
    }

    private static ResultSet getResultSet(Connection connection, String SQL) {
        ResultSet resultSet = null;
        try {
            resultSet = connection.createStatement().executeQuery(SQL);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return resultSet;
    }

    /**
     * Кладем что-то в БД
     */
    private static void executeSQL(String SQL) {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            statement.execute(SQL);
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DB_URL, "user", "");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

    private static boolean isRowExistsBySelect(String select) {
        Boolean isExist = null;
        try {
            isExist = (getResultSet(getConnection(), select) == null) ? false : (getResultSet(getConnection(), select).next()) ;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return isExist;
    }
}
