package сlinicbot.db;

import org.telegram.telegrambots.meta.api.objects.User;
import сlinicbot.entity.Clinic;
import сlinicbot.entity.Procedure;

import java.util.Formatter;

public class SQLStatements {
    /**
     * Добавление нового клиента
     *
     * @return String for Statement
     */
    public static String addNewClientSQL(Integer id, String firstName, Boolean isBot, String lastName, String userName, String languageCode) {
        Formatter formatter = new Formatter();
        return formatter.format("INSERT INTO USERS" +
                        " (ID, FIRST_NAME, IS_BOT, LAST_NAME, USER_NAME, LANGUAGE_CODE)" +
                        " VALUES (%d,'%s',%b,'%s','%s','%s')"
                , id, firstName, isBot, lastName, userName, languageCode).toString();
    }

    /**
     * Добавление нового клиента
     *
     * @return String for Statement
     */
    public static String addNewClientSQL(User user) {
        Formatter formatter = new Formatter();
        return formatter.format("INSERT INTO USERS" +
                        " (ID, FIRST_NAME, IS_BOT, LAST_NAME, USER_NAME, LANGUAGE_CODE)" +
                        " VALUES (%d,'%s','%b','%s','%s','%s')"
                , user.getId(), user.getFirstName(), user.getBot(),
                user.getLastName(), user.getUserName(),
                user.getLanguageCode()).toString();
    }

    /**
     * Добавляем клиенту город
     *
     * @return String for Statement
     */
    public static String setClientCitySQL(Integer id) {
        Formatter formatter = new Formatter();
        return formatter.format("UPDATE " +
                "USERS SET CITY WHERE ID=%d", id).toString();
    }

    /**
     * Добавляем новую запрошенную клиентом процедуру
     *
     * @return String for Statement
     */
    public static String addClientRequiredProcedure(Integer userId, String procedure) {
        Formatter formatter = new Formatter();
        return formatter.format("INSERT INTO REQUSETED_PROCEDURES " +
                        "(USER_ID, PROCEDURE) " +
                        "VALUES ('%d', '%s')",
                userId, procedure).toString();
    }

    /**
     * Добавляем новую клинику
     *
     * @return String for Statement
     */
    public static String addNewClinicWithoutProceduresList(Clinic clinic) {
        Formatter formatter = new Formatter();
        return formatter.format("INSERT INTO CLINICS " +
                        "(ADDRESS, NAME, ID)" +
                        "VALUES ('%s', '%s', '%s')",
                clinic.getAddress(), clinic.getName(), clinic.getId())
                .toString();
    }

    /**
     * Добавляем новый тип прроцедуры
     *
     * @return String for Statement
     */
    public static String addNewProcedureToProcedureTypes(Procedure procedure) {
        Formatter formatter = new Formatter();
        return formatter.format("INSERT INTO PROCEDURE_TYPES " +
                        "(ID, NAME, DEFAULT_PRICE)" +
                        " VALUES ('%s', '%s', '%s')",
                procedure.getId(), procedure.getName(), procedure.getPrice())
                .toString();
    }

    /**
     * Добавляем клинике новую процедуру
     *
     * @return String for Statement
     */
    public static String addClinicProcedure(Procedure procedure, Clinic clinic) {
        Formatter formatter = new Formatter();
        return formatter.format("INSERT INTO CLINIC_PROCEDURES" +
                        "(NAME, CLINIC_ID, PROCEDURE_ID)" +
                        "VALUES ('%s', '%s', '%s')",
                procedure.getName(), clinic.getId(),
                procedure.getId()).toString();
    }

    public static String simpleSelectWhere(String column, String whereColumn, String whereValue) {
        Formatter formatter = new Formatter();
        return formatter.format("SELECT * FROM %s WHERE %s='%s'", column, whereColumn, whereValue).toString();
    }

    public static String simpleSelectWhere(String column, String whereColumn, Integer whereValue) {
        Formatter formatter = new Formatter();
        return formatter.format("SELECT * FROM %s WHERE %s=%d", column, whereColumn, whereValue).toString();
    }
}
