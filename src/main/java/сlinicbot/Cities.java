package сlinicbot;

public enum Cities {
    MOSCOW("Москва"),
    CBP("СБП");

    private String city;

    Cities(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }
}
