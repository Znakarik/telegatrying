package сlinicbot.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "CLINICS")
public class Clinic extends AbstractEntity {
    @Id
    @NotNull String id = UUID.randomUUID().toString();
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "NAME")
    private String name;
    @OneToMany(mappedBy = "clinic", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<сlinicbot.entity.Procedure> procedures;

    public Clinic(String address, String name, сlinicbot.entity.Procedure... procedures) {
        this.address = address;
        this.name = name;
        this.procedures = Arrays.asList(procedures.clone());
    }

    public Clinic() {
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void addProcedure(сlinicbot.entity.Procedure procedure) {
        procedures.add(procedure);
    }

    enum Procedure {
        CLEAN_SKIN("Чистка лица"),
        LASER_EPILATION("Лазерная эпиляция"),
        IONIZATION("Ионизация"),
        MIOSTIMULATION("Миостимуляция"),
        LASER_BOIRELETIVISATION("Лазерная биоревитализация"),
        CRIOLIFTING("Криолифтинг"),
        PLASMOLIFTING("Плазмолифтинг"),
        VELA_SHAPE("Vela Shape"),
        PLASMAPHERESIS("Плазмаферез"),
        RF_LIFTING("RF-лифтинг"),
        ELOS_REJUVENATION("ELOS-омоложение"),
        PHOTO_REJUVENATION("Фотоомоложение"),
        OZONE_TERAPHY("Озонотерапия");

        private String procedure;

        Procedure(String procedure) {
            this.procedure = procedure;
        }

        public String getProcedure() {
            return procedure;
        }

        public void setProcedure(String procedure) {
            this.procedure = procedure;
        }
    }
}
