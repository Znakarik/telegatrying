package сlinicbot.entity.client;

import lombok.Data;
import org.telegram.telegrambots.meta.api.objects.User;
import сlinicbot.entity.Procedure;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "USERS")
public class Client {
    // TODO: 10/15/20 добавить FOREIGN KEY в таблицах для связей 
    @Id
    @Column(name = "ID")
    Integer id;
    @Column(name = "FIRST_NAME")
    String firstName;
    @Column(name = "IS_BOT")
    Boolean isBot;
    @Column(name = "LAST_NAME")
    String lastName;
    @Column(name = "USER_NAME")
    String userName;
    @Column(name = "LANGUAGE_CODE")
    String languageCode;
    @Column(name = "CITY")
    private String city;
    @Column(name = "ADDRESS")
    private String address;
    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Procedure> requestedProcedures = new ArrayList<>();

    protected Client() {
    }

    private Client(Integer id, String firstName, Boolean isBot, String lastName, String userName, String languageCode) {
        this.id = id;
        this.firstName = firstName;
        this.isBot = isBot;
        this.lastName = lastName;
        this.userName = userName;
        this.languageCode = languageCode;
    }

    public String getCity() {
        return city;
    }

    public List<Procedure> getRequestedProcedures() {
        return requestedProcedures;
    }

    public void addRequestedProcedure(Procedure requestedProcedure) {
        requestedProcedures.add(requestedProcedure);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Boolean getBot() {
        return isBot;
    }

    public void setBot(Boolean bot) {
        isBot = bot;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public static class ClientBuilder {
        public static Client build(User user) {
            return new Client(user.getId(), user.getFirstName(), user.getBot(), user.getLastName(), user.getUserName(), user.getLanguageCode());
        }

        public static Client build(Integer id, String firstName, Boolean isBot, String lastName, String userName, String languageCode) {
            return new Client(id, firstName, isBot, lastName, userName, languageCode);
        }
    }
}
