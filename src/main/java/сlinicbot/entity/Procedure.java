package сlinicbot.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Entity
@Table(name = "PROCEDURE_TYPES")
public class Procedure extends AbstractEntity {
    @Id
    @NotNull String id = UUID.randomUUID().toString();
    @Column(name = "NAME")
    String name;
    @Column(name = "DEFAULT_PRICE")
    Integer price;

    public Procedure(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public Procedure() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
