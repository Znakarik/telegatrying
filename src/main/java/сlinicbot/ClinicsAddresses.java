package сlinicbot;

public enum ClinicsAddresses {
    MOSCOW_FIRST("Куйбышева 10"),
    MOSCOW_SECOND("Голицина 2"),
    MOSCOW_THIRD("Косыгина 1"),
    SBP_FIRST("Крыжовникова 1"),
    SBP_SECOND("Колькина 1"),
    SBP_THIRD("Олькина 1");

    private String address;

    ClinicsAddresses(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public boolean incomeAddressExists(String incomeAddress){
        return ClinicsAddresses.valueOf(incomeAddress) != null;
    }
}
