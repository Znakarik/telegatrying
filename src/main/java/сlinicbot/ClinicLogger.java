package сlinicbot;


import java.io.FileInputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class ClinicLogger {
    public static Logger LOGGER;

    static {
        try (FileInputStream ins = new FileInputStream("src/main/java/сlinicbot/log.config")) {
            LogManager.getLogManager().readConfiguration(ins);
            LOGGER = Logger.getLogger(ClinicLogger.class.getName());
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }
}
